# Fetching the minified node image on alpine linux
FROM node:lts-alpine

# Declaring env
ENV NODE_ENV development

# Setting up the work directory
WORKDIR /

# Copying all the files in our project
COPY . .

# Installing dependencies
RUN npm install

# Starting our application
CMD [ "node", "server.js" ]