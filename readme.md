# Build docker image to deploy
1. `docker build -f Dockerfile -t registry.gitlab.com/michael.cojocari/rhbot .`

    (for my RPI 5)

    `docker build --platform=linux/arm64/v8 -f Dockerfile -t registry.gitlab.com/michael.cojocari/rhbot .`

2. `docker tag registry.gitlab.com/michael.cojocari/rhbot:latest registry.gitlab.com/michael.cojocari/rhbot:latest`

3. `docker push registry.gitlab.com/michael.cojocari/rhbot:latest`

# Run docker image on my RPI5 via CLI (or just create it through portainer)
1. `sudo docker pull registry.gitlab.com/michael.cojocari/rhbot:latest`
2. `sudo docker run registry.gitlab.com/michael.cojocari/rhbot:latest` 