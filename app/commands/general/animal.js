const { SlashCommandBuilder } = require("discord.js");
const animals = require("@sefinek/random-animals");

module.exports = {
  data: new SlashCommandBuilder()
    .setName("animal")
    .setDescription("Retrieves a random animal image.")
    .addStringOption((option) =>
      option
        .setName("type")
        .setDescription("Type of animal you would like to see.")
        .setRequired(true)
        .addChoices(
          { name: "cat", value: "cat" },
          { name: "dog", value: "dog" },
          { name: "fox", value: "fox" },
          { name: "fish", value: "fish" },
          { name: "alpaca", value: "alpaca" },
          { name: "bird", value: "bird" },
          { name: "random", value: "random" },
        ),
    ),
  async execute(interaction) {
    console.log(
      `[INFO] Someone has called animal with parameter ${interaction.options.getString(
        "type",
      )}`,
    );
    callAnimal(interaction.options.getString("type"))
      .then(async (animalUrl) => {
        await interaction.reply({ content: animalUrl.message });
      })
      .catch(async (error) => {
        console.error(error);
        // await interaction.reply({ content: "The animal API is buggy." });
      });
  },
};

const callAnimal = async (type) => {
  if (type === "random") {
    const animalTypes = ["cat", "dog", "fox", "fish", "alpaca", "bird"];
    type = animalTypes[Math.floor(Math.random() * animalTypes.length)]; // The maximum is exclusive and the minimum is inclusive
  }

  switch (type) {
    case "cat":
      return animals.cat();
    case "dog":
      return animals.dog();
    case "fox":
      return animals.fox();
    case "fish":
      return animals.fish();
    case "alpaca":
      return animals.alpaca();
    case "bird":
      return animals.bird();
    default:
      return animals.alpaca();
  }
};
