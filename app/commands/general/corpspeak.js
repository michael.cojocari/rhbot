const {SlashCommandBuilder} = require("discord.js");

const firstSet= ['proactively', 'asynchronously', 'synergistically', 'seamlessly'];
const secondSet = ['circle back on', 'leverage', 'align on', 'actualize', 'deep dive', 'pivot'];
const thirdSet = ['stakeholders', 'opportunities', 'ROI', 'deliverables', 'bandwidth', 'strategy', 'core competencies'];

module.exports = {
  data: new SlashCommandBuilder()
    .setName("corpspeak")
    .setDescription("Random corporate speak."),
  async execute(interaction) {
    console.log('[INFO] Someone called corpspeak.');
    await interaction.reply({ content: compileCorpSpeak() });
  }
}

const compileCorpSpeak = () => {
  const first = Math.floor(Math.random() * firstSet.length);
  const second = Math.floor(Math.random() * secondSet.length);
  const third = Math.floor(Math.random() * thirdSet.length);

  return firstSet[first] + " " + secondSet[second] + " " + thirdSet[third];
}