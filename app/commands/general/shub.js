const { SlashCommandBuilder } = require("discord.js");

module.exports = {
  data: new SlashCommandBuilder()
    .setName("shub")
    .setDescription("shub sends a shub shub"),
  async execute(interaction) {
    console.log(`[INFO] Shub is to be used for debug purposes only.`);
    await interaction.reply({ content: "Shub Shub", ephemeral: true });
  },
};
