const { SlashCommandBuilder, EmbedBuilder } = require("discord.js");
const { application } = require("express");
const request = require("request");

module.exports = {
  data: new SlashCommandBuilder()
    .setName("weather")
    .setDescription("Sends quick weather data.")
    .addStringOption((option) =>
      option
        .setName("zipcode")
        .setDescription("ZIP code or use the default.")
        .setRequired(false),
    ),
  async execute(interaction) {
    const zipcode =
      interaction.options.getString("zipcode") || process.env.DEFAULT_ZIPCODE;

    const openWeatherUri =
      "http://api.openweathermap.org/data/2.5/weather?" +
      "appid=" +
      process.env.OPENWEATHERMAP_APIKEY +
      "&zip=" +
      zipcode;
    console.log(`[INFO] User '${interaction.user.username}' called weather.`);

    request(openWeatherUri, async (error, response, body) => {
      if (!error && response.statusCode == 200) {
        const weatherData = JSON.parse(body);
        const feelsLike = Math.ceil(
          ((weatherData["main"]["feels_like"] - 273.15) * 9) / 5 + 32,
        );
        const weatherEmbed = new EmbedBuilder()
          .setColor(0x0099ff)
          .setTitle(`${weatherData["name"]}`)
          .addFields(
            { name: "Feels like", value: `${feelsLike}F` },
            { name: "Wind is", value: `${weatherData["wind"]["speed"]}mph` },
            {
              name: "Humidity at",
              value: `${weatherData["main"]["humidity"]}`,
            },
          );

        await interaction.reply({ embeds: [weatherEmbed] });
      } else {
        await interaction.reply({
          content: `Could not find any weather information for ${zipcode}.`,
          ephemeral: true,
        });
      }
    });
  },
};
