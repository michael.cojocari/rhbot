const { SlashCommandBuilder } = require("discord.js");

module.exports = {
  data: new SlashCommandBuilder()
    .setName("duck")
    .setDescription("Duck videos.")
    .addStringOption((option) =>
      option
        .setName("number")
        .setDescription("Which video do you wish to torment us with?")
        .setRequired(false)
        .addChoices(
          { name: "1", value: "1" },
          { name: "2", value: "2" },
          { name: "3", value: "3" },
          { name: "4", value: "4" },
        ),
    ),
  async execute(interaction) {
    console.log(`[INFO] Someone is calling forth the duck.`);

    let duckChoice = interaction.options.getString("number");
    if (!duckChoice) {
      const duckChoices = ["1", "2", "3", "4", "5"];
      duckChoice = duckChoices[Math.floor(Math.random() * duckChoices.length)];
    }

    let url = "";
    switch (duckChoice) {
      case "1":
        url = "https://www.youtube.com/watch?v=MtN1YnoL46Q";
        break;
      case "2":
        url = "https://www.youtube.com/watch?v=7jjcAuEYW9M";
        break;
      case "3":
        url = "https://www.youtube.com/watch?v=Ru4a-js4My4";
        break;
      case "4":
        url = "https://www.youtube.com/watch?v=1oQfagXfBqE";
        break;
      case "5":
        url = "https://www.youtube.com/watch?v=DSG53BsUYd0";
        break;
      default:
        url = "https://www.youtube.com/watch?v=MtN1YnoL46Q";
    }

    await interaction.reply({ content: url });
  },
};
