// const { SlashCommandBuilder, EmbedBuilder } = require("discord.js");
// const speedTest = require("speedtest-net");
//
// module.exports = {
//   data: new SlashCommandBuilder()
//     .setName("speedtest")
//     .setDescription("Runs speedtest on my home network."),
//   async execute(interaction) {
//     console.log(`[INFO] Initiating speedtest.`);
//     await interaction.deferReply();
//     speedTest({ acceptLicense: true })
//       .then(async (response) => {
//         console.log(response);
//
//         const uploadMbs =
//           (response["upload"]["bytes"] * 8) /
//           ((1 / Math.pow(10, 3)) * response["upload"]["elapsed"]) /
//           Math.pow(10, 6);
//         const downloadMbs =
//           (response["download"]["bytes"] * 8) /
//           ((1 / Math.pow(10, 3)) * response["download"]["elapsed"]) /
//           Math.pow(10, 6);
//
//         const speedTestEmbed = new EmbedBuilder()
//           .setColor(0x0099ff)
//           .setTitle("Speedtest Results")
//           .addFields(
//             { name: "Server", value: `${response["server"]["name"]}` },
//             { name: "Ping Jitter", value: `${response["ping"]["jitter"]}` },
//             { name: "Ping Latency", value: `${response["ping"]["latency"]}` },
//             { name: "Download", value: `${Math.ceil(downloadMbs)} Mbs` },
//             { name: "Upload", value: `${Math.ceil(uploadMbs)} Mbs` },
//           );
//         await interaction.editReply("Speedtest complete.");
//         await interaction.followUp({ embeds: [speedTestEmbed] });
//       })
//       .catch(async (error) => {
//         console.error(error);
//         await interaction.editReply("Speedtest stubbed its toe.");
//       });
//   },
// };
