require("dotenv").config();
const sqlite3 = require("sqlite3").verbose();
const fs = require("node:fs");
const path = require("node:path");
const { Client, Collection, Events, GatewayIntentBits } = require("discord.js");

const discord = require("discord.js");

const rhDb = new sqlite3.Database(process.env.RH_DB, (error) => {
  if (error) {
    console.error(error);
  } else {
    console.log("[INFO] Connected to DB");
    console.log("[INFO] Users in database: ");
    rhDb.all(
      "SELECT discord_user_name FROM discord_users",
      [],
      (error, discordUserNames) => {
        discordUserNames.forEach((username) => {
          console.log(`[INFO] ${username.discord_user_name}`);
        });
      },
    );
  }
});

const client = new Client({
  intents: [GatewayIntentBits.Guilds, GatewayIntentBits.GuildMessages],
});

client.commands = new Collection();
const foldersPath = path.join(__dirname, "app/commands");
const commandFolders = fs.readdirSync(foldersPath);

commandFolders.forEach((folder) => {
  const commandsPath = path.join(foldersPath, folder);
  const commandFiles = fs
    .readdirSync(commandsPath)
    .filter((file) => file.endsWith(".js"));
  for (const file of commandFiles) {
    const filePath = path.join(commandsPath, file);
    const command = require(filePath);
    if ("data" in command && "execute" in command) {
      client.commands.set(command.data.name, command);
    } else {
      console.log(
        `[WARNING] The command at ${filePath} is missing a required "data" or "execute" property.`,
      );
    }
  }
});

client.once(Events.ClientReady, () => {
  console.log(`[INFO] Logged in as ${client.user.tag}`);
});

client.on(Events.InteractionCreate, async (interaction) => {
  if (!interaction.isChatInputCommand()) return;

  const command = interaction.client.commands.get(interaction.commandName);

  if (!command) {
    console.error(`No command matching ${interaction.commandName} was found.`);
    return;
  }

  try {
    await command.execute(interaction);
  } catch (error) {
    console.error(error);
    if (interaction.replied || interaction.deferred) {
      await interaction.followUp({
        content: "There was an error while executing this command!",
        ephemeral: true,
      });
    } else {
      await interaction.reply({
        content: "There was an error while executing this command!",
        ephemeral: true,
      });
    }
  }
});

client.login(process.env.DISCORD_TOKEN);
